# Это игра по угадыванию чисел.
import random

guessesTaken = 0  # количество попыток

print('Привет! Как тебя зовут?')
myName = input()

number = random.randint(1, 20)
print("Что ж, " + myName + ", я загадываю число от 1 до 20.")

for guessesTaken in range(6):
    print('Попробуй угадать.')
    guess = int(input())

    if guess < number:
        print('Твоё число слишком мало.')
    elif guess > number:
        print('Твоё число слишком велико.')
    elif guess == number:
        break

if guess == number:
    guessesTaken = str(guessesTaken + 1)
    print('Отлично, ' + myName + '! Ты справился за ' + guessesTaken + ' попытки!')

if guess != number:
    number = str(number)
    print('Увы. Было загадано число ' + number + '.')
